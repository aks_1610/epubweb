﻿using lib;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

public partial class simple_document : System.Web.UI.Page
{
    protected Config configManager;
    private HttpClient objHttpClient = null;
    private OpenPubDTO objOpenPubDTO = null;
    private HttpResponseMessage openRes = null;
    protected async void Page_Load(object sender, EventArgs e)
    {
        try
        {
            configManager = new Config(Server.MapPath(VirtualPathUtility.GetDirectory(Request.Path)));

            if (!Page.IsPostBack && !Page.IsCallback)
            {
                if (Session["APIClasses"] != null)
                {
                    objHttpClient = (Session["APIClasses"] as APIClasses).HttpClient;
                    if (!(Session["APIClasses"] as APIClasses).isPublicationOpened) //Check if publication is opened, and prohibits incrementing the counter again and again
                    {
                        if (!await OpenPublication())
                        {
                            Response.Write("Publication can not be opened as the session might be invalid or all the licenses for the publication are in use.");
                            Response.End();
                        }
                        else
                        {
                            if ((Session["APIClasses"] as APIClasses).SecretModeOn)
                            {
                                pageDiv.Visible = false;
                            }
                            else
                            {
                                APIClasses objAPIClasses = (Session["APIClasses"] as APIClasses);
                                pageDiv.Visible = true;
                                lblUser.InnerText = String.Format("{0} {1}", objAPIClasses.User.firstName.ToUpper(), objAPIClasses.User.lastName.ToUpper());
                                userImg.Alt = lblUser.InnerText;
                            }
                        }
                    }
                    else
                    {
                        if ((Session["APIClasses"] as APIClasses).SecretModeOn)
                        {
                            pageDiv.Visible = false;
                        }
                        else
                        {
                            APIClasses objAPIClasses = (Session["APIClasses"] as APIClasses);
                            pageDiv.Visible = true;
                            lblUser.InnerText = String.Format("{0} {1}", objAPIClasses.User.firstName.ToUpper(), objAPIClasses.User.lastName.ToUpper());
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    /// <summary>
    /// OpenPublication
    /// </summary>
    /// <returns></returns>
    private async Task<bool> OpenPublication()
    {
        bool isAuthenticated = false;
        try
        {
            string strOpenPublicationURL = ConfigurationManager.AppSettings.Get("OpenPublicationURL");

            objOpenPubDTO = new OpenPubDTO
            {
                PublicationId = (Session["APIClasses"] as APIClasses).PublicationId,
                UserId = (Session["APIClasses"] as APIClasses).User.id,
                CompanyId = (Session["APIClasses"] as APIClasses).User.companyID,
            };

            StringContent content = new StringContent(JsonConvert.SerializeObject(objOpenPubDTO), Encoding.UTF8, "application/json");

            using (HttpResponseMessage responseOpen = await objHttpClient.PostAsync(strOpenPublicationURL, content))
            {
                openRes = responseOpen;
                Debug.Assert(true);

                if (openRes.IsSuccessStatusCode)
                {
                    string data = await responseOpen.Content.ReadAsStringAsync();

                    //Saving the JSON as to be used in the windows application
                    openPublicationJSONDTO.Value = data;

                    objOpenPubDTO = JsonConvert.DeserializeObject<OpenPubDTO>(data);

                    (Session["APIClasses"] as APIClasses).OpenPubDTO = objOpenPubDTO;

                    isAuthenticated = true;

                    (Session["APIClasses"] as APIClasses).isPublicationOpened = true;
                }
                else
                {
                    isAuthenticated = false;
                }
            }            
        }
        catch (Exception ex)
        {
            isAuthenticated = false;
            throw;
        }

        return isAuthenticated;
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("ClosePublication.aspx", false);

        HttpContext.Current.ApplicationInstance.CompleteRequest();
    }
}