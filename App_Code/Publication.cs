﻿/*******************************************************************************
 *                          © 2017 vLinkd Australia                            *
 *                          All Rights Reserved.                               *
 *                                                                             *
 *******************************************************************************
 *
 * Modification History:
 * 
 * AKS 09Oct2017 Created the class
 *******************************************************************************/

/// <summary>
/// Publication
/// </summary>
public class Publication
{
    /// <summary>
    /// publicationId
    /// </summary>
    public int publicationId { get; set; }

    /// <summary>
    /// pubTitle
    /// </summary>
    public string pubTitle { get; set; }

    /// <summary>
    /// author
    /// </summary>
    public string author { get; set; }

    /// <summary>
    /// editionYear
    /// </summary>
    public int editionYear { get; set; }

    /// <summary>
    /// edition
    /// </summary>
    public string edition { get; set; }

    /// <summary>
    /// image
    /// </summary>
    public string image { get; set; }

    /// <summary>
    /// price
    /// </summary>
    public double price { get; set; }

    /// <summary>
    /// discount
    /// </summary>
    public int discount { get; set; }

    /// <summary>
    /// publicationCategoryId
    /// </summary>
    public int publicationCategoryId { get; set; }

    /// <summary>
    /// publicationCategoryName
    /// </summary>
    public string publicationCategoryName { get; set; }

    /// <summary>
    /// pubFilePath
    /// </summary>
    public string pubFilePath { get; set; }

    /// <summary>
    /// pubFilename
    /// </summary>
    public string pubFilename { get; set; }

    /// <summary>
    /// pubImagename
    /// </summary>
    public string pubImagename { get; set; }

    /// <summary>
    /// watermarkOn
    /// </summary>
    public bool watermarkOn { get; set; }

    /// <summary>
    /// watermarkText
    /// </summary>
    public string watermarkText { get; set; }

    /// <summary>
    /// secretModeOn
    /// </summary>
    public bool secretModeOn { get; set; }

    /// <summary>
    /// watermarkBarOn
    /// </summary>
    public bool watermarkBarOn { get; set; }

    /// <summary>
    /// watermarkBarText
    /// </summary>
    public string watermarkBarText { get; set; }

    /// <summary>
    /// active
    /// </summary>
    public bool active { get; set; }

    /// <summary>
    /// paymentGatewayBuyNowScript
    /// </summary>
    public string paymentGatewayBuyNowScript { get; set; }

    /// <summary>
    /// isPubSupplementary
    /// </summary>
    public bool isPubSupplementary { get; set; }

    /// <summary>
    /// parentPubId
    /// </summary>
    public int parentPubId { get; set; }

    /// <summary>
    /// publicationFileContents
    /// </summary>
    public object publicationFileContents { get; set; }

    /// <summary>
    /// publicationImageContents
    /// </summary>
    public object publicationImageContents { get; set; }

    /// <summary>
    /// accessNowLink
    /// </summary>
    public string accessNowLink { get; set; }

    /// <summary>
    /// lastUpdated
    /// </summary>
    public string lastUpdated { get; set; }

    /// <summary>
    /// lastUpdatedBy
    /// </summary>
    public object lastUpdatedBy { get; set; }

    /// <summary>
    /// createdOn
    /// </summary>
    public string createdOn { get; set; }

    /// <summary>
    /// createdBy
    /// </summary>
    public object createdBy { get; set; }
}