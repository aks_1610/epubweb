﻿/*******************************************************************************
 *                          © 2017 vLinkd Australia                            *
 *                          All Rights Reserved.                               *
 *                                                                             *
 *******************************************************************************
 *
 * Modification History:
 * 
 * AKS 09Oct2017 Created the class
 *******************************************************************************/

using System;

/// <summary>
/// Class for converting JSON response
/// </summary>
public class ClientTokenKey
{
    /// <summary>
    /// tokenKey
    /// </summary>
    public string tokenKey { get; set; }

    /// <summary>
    /// issuedOn
    /// </summary>
    public DateTime issuedOn { get; set; }

    /// <summary>
    /// expiresOn
    /// </summary>
    public DateTime expiresOn { get; set; }

    /// <summary>
    /// tokenType
    /// </summary>
    public string tokenType { get; set; }

    /// <summary>
    /// createdOn
    /// </summary>
    public DateTime createdOn { get; set; }

    /// <summary>
    /// clientKeyID
    /// </summary>
    public string clientKeyID { get; set; }

    /// <summary>
    /// userID
    /// </summary>
    public string userID { get; set; }

    /// <summary>
    /// id
    /// </summary>
    public int id { get; set; }
}