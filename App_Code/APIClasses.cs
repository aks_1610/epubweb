﻿using System.Net.Http;

/// <summary>
/// Summary description for APIClasses
/// </summary>
public class APIClasses
{
    /// <summary>
    /// User
    /// </summary>
    public User User { get; set; }

    /// <summary>
    /// Publication
    /// </summary>
    public Publication Publication { get; set; }

    /// <summary>
    /// UserToken
    /// </summary>
    public string UserToken { get; set; }

    /// <summary>
    /// UserToken
    /// </summary>
    public string ClientToken { get; set; }

    /// <summary>
    /// PublicationId
    /// </summary>
    public int PublicationId { get; set; }

    /// <summary>
    /// BaseAddressV1
    /// </summary>
    public string BaseAddressV1 { get; set; }

    /// <summary>
    /// HttpClient
    /// </summary>
    public HttpClient HttpClient { get; set; }

    /// <summary>
    /// SecretModeOn
    /// </summary>
    public bool SecretModeOn { get; set; }

    /// <summary>
    /// OpenPubDTO
    /// </summary>
    public OpenPubDTO OpenPubDTO { get; set; }

    /// <summary>
    /// SessionExpireTime
    /// </summary>
    public int SessionExpireTime { get; set; }

    /// <summary>
    /// ConfigParameter
    /// </summary>
    public ConfigParameter ConfigParameter { get; set; }

    /// <summary>
    /// isPublicationOpened
    /// </summary>
    public bool isPublicationOpened { get; set; }
}