﻿using Newtonsoft.Json;

/// <summary>
/// Summary description for OpenPubDTO
/// </summary>
public class OpenPubDTO
{
    [JsonProperty("PublicationId")]
    public int PublicationId { get; set; }

    /// <summary>
    /// UserId
    /// </summary>
    [JsonProperty("UserId")]
    public int UserId { get; set; }

    [JsonProperty("CompanyId")]
    public int CompanyId { get; set; }

    [JsonProperty("EPubDocUrl")]
    public string EPubDocUrl { get; set; }

    [JsonProperty("PubViewLogId")]
    public int PubViewLogId { get; set; }

    [JsonProperty("CompanySubscriptionId")]
    public int CompanySubscriptionId { get; set; }
}