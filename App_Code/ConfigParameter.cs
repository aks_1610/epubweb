﻿using System;

/// <summary>
/// Summary description for ConfigParameter
/// </summary>
public class ConfigParameter
{
    public int configParameterId { get; set; }

    public string parameterKey { get; set; }

    public string parameterValue { get; set; }

    public string parameterPurpose { get; set; }

    public DateTime? lastUpdated { get; set; }

    public string lastUpdatedBy { get; set; }

    public DateTime? createdDate { get; set; }

    public string createdBy { get; set; }
}