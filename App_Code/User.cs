﻿/*******************************************************************************
 *                          © 2017 vLinkd Australia                            *
 *                          All Rights Reserved.                               *
 *                                                                             *
 *******************************************************************************
 *
 * Modification History:
 * 
 * AKS 09Oct2017 Created the class
 *******************************************************************************/

/// <summary>
/// Summary description for User
/// </summary>
public class User
{

    //Id
    public int id { get; set; }

    /// <summary>
    /// firstName
    /// </summary>
    public string firstName { get; set; }

    /// <summary>
    /// lastName
    /// </summary>
    public string lastName { get; set; }

    /// <summary>
    /// roleID
    /// </summary>
    public int roleID { get; set; }

    /// <summary>
    /// roleName
    /// </summary>
    public string roleName { get; set; }

    /// <summary>
    /// login
    /// </summary>
    public string login { get; set; }

    /// <summary>
    /// companyID
    /// </summary>
    public int companyID { get; set; }

    /// <summary>
    /// companyName
    /// </summary>
    public string companyName { get; set; }

    /// <summary>
    /// password
    /// </summary>
    public string password { get; set; }

    /// <summary>
    /// department
    /// </summary>
    public string department { get; set; }

    /// <summary>
    /// accessGrantedUntil
    /// </summary>
    public string accessGrantedUntil { get; set; }

    /// <summary>
    /// phone
    /// </summary>
    public string phone { get; set; }

    /// <summary>
    /// mobile
    /// </summary>
    public string mobile { get; set; }

    /// <summary>
    /// picture
    /// </summary>
    public string picture { get; set; }

    /// <summary>
    /// pictureName
    /// </summary>
    public string pictureName { get; set; }

    /// <summary>
    /// pictureContents
    /// </summary>
    public string pictureContents { get; set; }

    /// <summary>
    /// active
    /// </summary>
    public bool active { get; set; }

    /// <summary>
    /// createdFromPaymentGatewayOrderId
    /// </summary>
    public int createdFromPaymentGatewayOrderId { get; set; }

    /// <summary>
    /// createdFromPaymentGatewayOrderNumber
    /// </summary>
    public string createdFromPaymentGatewayOrderNumber { get; set; }

    /// <summary>
    /// lastUpdated
    /// </summary>
    public string lastUpdated { get; set; }

    /// <summary>
    /// lastUpdatedBy
    /// </summary>
    public string lastUpdatedBy { get; set; }

    /// <summary>
    /// createdOn
    /// </summary>
    public string createdOn { get; set; }

    /// <summary>
    /// createdBy
    /// </summary>
    public string createdBy { get; set; }
}