﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;

public partial class ExtendSession : Page
{
    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender">object</param>
    /// <param name="e">EventArgs</param>
    protected async void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            if (await ExtendUserSession())
            {
                Response.Write("User session extended successfully.");
            }
            else
            {
                Response.Write("User session can not be extended successfully.");
            }
        }
    }

    /// <summary>
    /// ExtendSession
    /// </summary>
    /// <returns>Boolean</returns>
    //[System.Web.Services.WebMethod]
    public async Task<bool> ExtendUserSession()
    {
        bool isUserSessionExtended = false;

        if (Session["APIClasses"] != null)
        {
            HttpClient objHttpClient = (Session["APIClasses"] as APIClasses).HttpClient;

            string strExtendUserSessionURL = ConfigurationManager.AppSettings.Get("ExtendUserSessionURL");

            if (null != objHttpClient && !String.IsNullOrWhiteSpace(strExtendUserSessionURL))
            {
                var objExtendUserSession = new
                {
                    userToken = (Session["APIClasses"] as APIClasses).UserToken
                };

                StringContent content = new StringContent(JsonConvert.SerializeObject(objExtendUserSession), Encoding.UTF8, "application/json");

                using (HttpResponseMessage responseUpdate = await objHttpClient.PostAsync(strExtendUserSessionURL, content))
                {
                    if (responseUpdate.IsSuccessStatusCode)
                    {
                        string data = await responseUpdate.Content.ReadAsStringAsync();
                        ClientTokenKey objClientTokenKey = JsonConvert.DeserializeObject<ClientTokenKey>(data);

                        objHttpClient.DefaultRequestHeaders.Clear();

                        (Session["APIClasses"] as APIClasses).UserToken = objClientTokenKey.tokenKey;

                        //Add authentication headers
                        objHttpClient.DefaultRequestHeaders.Add("TokenKey", (Session["APIClasses"] as APIClasses).ClientToken);
                        objHttpClient.DefaultRequestHeaders.Add("UserToken", objClientTokenKey.tokenKey);

                        (Session["APIClasses"] as APIClasses).HttpClient = objHttpClient;

                        //Session.Timeout = (Session["APIClasses"] as APIClasses).SessionExpireTime;
                        isUserSessionExtended = true;
                    }
                    else
                    {
                        isUserSessionExtended = false;
                    }
                }
            }
        }
        return isUserSessionExtended;
    }
}