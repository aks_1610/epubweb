﻿<%@ Page Async="true" Language="C#" AutoEventWireup="true" CodeFile="ClosePublication.aspx.cs" Inherits="ClosePublication" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
    <meta http-equiv="EXPIRES" content="0">
    <title>Rawlinsons</title>
    <script type="text/javascript">
        function CloseWindow()
        {
            window.self.close();
        }
        function TryAgain()
        {
            location.reload();
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align:center" id="divMessage" runat="server">
        <h3 runat="server" id="closePubH">Closing the publication...</h3>
        <input type="button" onclick="CloseWindow()" id="btnClose" value="Close" runat="server" visible="false" />
    </div>
    </form>
</body>
</html>
