<%@ WebHandler Language="C#" Class="view" %>

using System;
using System.Web;
using lib;
using System.IO;

public class view : IHttpHandler
{
    protected lib.Config configManager;
    protected String doc = "";
    protected String stamp = "";
    protected String print = "";
    protected String pdfdoc = "";
    protected annotate annotator;
    protected String identifier = "";

    public void ProcessRequest(HttpContext context)
    {
        doc     = context.Request["doc"].ToLower();
        stamp   = context.Request["stamp"];

        if (context.Request["print"] != null){
            print   = context.Request["print"].ToLower();
        }

        if (!doc.EndsWith(".pdf")) { pdfdoc = doc + ".pdf"; } else { pdfdoc = doc; }

        annotator = new annotate(context.Server.MapPath(VirtualPathUtility.GetDirectory(context.Request.Path))+@"..\");
        String messages = annotator.convert(pdfdoc,stamp);

        if(messages.IndexOf("[OK") == 0){
            context.Response.ContentType = "application/pdf";
            identifier = messages.Substring(5);
            identifier = identifier.Substring(0,identifier.Length-2);

            context.Response.Write("services/view.ashx?doc=" + doc + "_annotated" + identifier + ".pdf" + "&format=pdf&marked=true" +  "&page=-1&download=" + (print=="true"?"false":"true"));
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}