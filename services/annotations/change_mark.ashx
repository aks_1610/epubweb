﻿<%@ WebHandler Language="C#" Class="change_mark" %>

using System;
using System.Web;
using lib;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Runtime.Serialization;

public class change_mark : IHttpHandler
{
    protected lib.Config configManager;
    protected JSONDict mark;
    private string _id;
    private HttpContext _c;

    public void ProcessRequest(HttpContext context)
    {
        configManager = new Config(context.Server.MapPath(VirtualPathUtility.GetDirectory(context.Request.Path)) + @"..\..\");
        mark = change_mark.Deserialise<JSONDict>(context.Request["MARK"]);
        int CompanyId = 0, Pubid = 0, UserId=0;
        if (context.Request["companyId"] != null) { CompanyId = int.Parse(context.Request["companyId"]); }
        if (context.Request["publicationId"] != null) { Pubid = int.Parse(context.Request["publicationId"]); }
        if (context.Request["userId"] != null) { UserId = int.Parse(context.Request["userId"]); }
        try
        {
            using (SqlConnection conn = new SqlConnection(configManager.getConfig("sql.connectionstring")))
            {
                conn.Open();

                SqlCommand nonqueryCommand = conn.CreateCommand();
                nonqueryCommand.CommandText = "UPDATE Annotation SET " +
                                                "document_filename = @document_filename, " +
                                                "document_relative_path = @document_relative_path, " +
                                                "selection_text = @selection_text, " +
                                                "has_selection = @has_selection, " +
                                                "color = @color, " +
                                                "selection_info = @selection_info, " +
                                                "readonly = @readonly, " +
                                                "displayFormat = @displayFormat, " +
                                                "note = @note, " +
                                                "pageIndex = @pageIndex, " +
                                                "positionX = @positionX, " +
                                                "positionY = @positionY, " +
                                                "width = @width, " +
                                                "height = @height, " +
                                                "collapsed = @collapsed, " +
                                                "points = @points, " +
                                                "companyId = @companyId, " +
                                                "UserId = @UserId, " +
                                                "PubId = @PubId, " +
                                                "LastUpdated = @LastUpdated, " +
                                                "LastUpdatedBy = @LastUpdatedBy " +
                                                "WHERE id = @id";

                nonqueryCommand.Parameters.AddWithValue("@id", getDictVariable("id"));
                nonqueryCommand.Parameters.AddWithValue("@document_filename", context.Request["DOCUMENT_FILENAME"]);
                nonqueryCommand.Parameters.AddWithValue("@document_relative_path", getDictVariable("XX"));
                nonqueryCommand.Parameters.AddWithValue("@selection_text", getDictVariable("selection_text"));
                nonqueryCommand.Parameters.AddWithValue("@has_selection", (getDictVariable("has_selection") == "true") ? 1 : 0);
                nonqueryCommand.Parameters.AddWithValue("@color", getDictVariable("color"));
                nonqueryCommand.Parameters.AddWithValue("@selection_info", getDictVariable("selection_info"));
                nonqueryCommand.Parameters.AddWithValue("@readonly", (getDictVariable("readonly") == "true") ? 1 : 0);
                nonqueryCommand.Parameters.AddWithValue("@type", getDictVariable("type"));
                nonqueryCommand.Parameters.AddWithValue("@displayFormat", getDictVariable("displayFormat"));
                nonqueryCommand.Parameters.AddWithValue("@note", getDictVariable("note"));
                nonqueryCommand.Parameters.AddWithValue("@pageIndex", getDictVariable("pageIndex"));
                nonqueryCommand.Parameters.AddWithValue("@positionX", getDictVariable("positionX"));
                nonqueryCommand.Parameters.AddWithValue("@positionY", getDictVariable("positionY"));
                nonqueryCommand.Parameters.AddWithValue("@width", getDictVariable("width"));
                nonqueryCommand.Parameters.AddWithValue("@height", getDictVariable("height"));
                nonqueryCommand.Parameters.AddWithValue("@collapsed", (getDictVariable("collapsed") == "true") ? 1 : 0);
                nonqueryCommand.Parameters.AddWithValue("@points", getDictVariable("points"));
                nonqueryCommand.Parameters.AddWithValue("@companyId", CompanyId);
                nonqueryCommand.Parameters.AddWithValue("@UserId", UserId);
                nonqueryCommand.Parameters.AddWithValue("@Pubid", Pubid);
                nonqueryCommand.Parameters.AddWithValue("@LastUpdated", DateTime.Now);
                nonqueryCommand.Parameters.AddWithValue("@LastUpdatedBy", UserId);

                nonqueryCommand.ExecuteNonQuery();
                context.Response.Write("1");
            }
        }
        catch (Exception ex)
        {
            context.Response.Write("0");
        }
    }

    private string getDictVariable(string key)
    {
        if (mark.dict.ContainsKey(key))
        {
            return mark.dict[key].ToString();
        }
        else
        {
            return "";
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public static JSONDict Deserialise<T>(string json)
    {
        JSONDict obj = new JSONDict();
        using (System.IO.MemoryStream ms = new System.IO.MemoryStream(System.Text.Encoding.Unicode.GetBytes(json)))
        {
            System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            obj = (JSONDict)serializer.ReadObject(ms);
            return obj;
        }
    }

    [Serializable]
    public class JSONDict : ISerializable
    {
        public Dictionary<string, string> dict;
        public JSONDict()
        {
            dict = new Dictionary<string, string>();
        }
        protected JSONDict(SerializationInfo info, StreamingContext context)
        {
            dict = new Dictionary<string, string>();
            foreach (var entry in info)
            {
                dict.Add(entry.Name, (entry.Value!=null)?entry.Value.ToString():"");
            }
        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            foreach (string key in dict.Keys)
            {
                info.AddValue(key, dict[key]);
            }
        }
    }
}

