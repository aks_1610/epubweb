﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.HtmlControls;

public partial class ClosePublication : System.Web.UI.Page
{
    protected async void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            if (await Decrement())
            {
                closePubH.InnerHtml = "<font color='green'>You have been successfully logged out. <BR />Publication closed successfully.</font>";
                Session["APIClasses"] = null;
                Session.Abandon();
                btnClose.Visible = true;
                Session["Closed"] = true;
            }
            else
            {
                closePubH.InnerHtml = "<font color='red'>Publication not closed successfully.</font>";

                HtmlAnchor objHtmlAnchor = new HtmlAnchor { ID = "hrefTryAgain", HRef = "#", InnerText = "Try Again" };
                objHtmlAnchor.Attributes.Add("onclick", "TryAgain()");

                divMessage.Controls.Add(objHtmlAnchor);
                btnClose.Visible = false;
            }
        }
    }

    /// <summary>
    /// Decrement
    /// </summary>
    /// <returns>Boolean</returns>
    public async Task<bool> Decrement()
    {
        bool isPublicationClosed = false;

        if (Session["APIClasses"] != null)
        {
            HttpClient objHttpClient = (Session["APIClasses"] as APIClasses).HttpClient;
            

            string strClosePublicationURL = ConfigurationManager.AppSettings.Get("ClosePublicationURL");

            if (null != objHttpClient && !String.IsNullOrWhiteSpace(strClosePublicationURL))
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject((Session["APIClasses"] as APIClasses).OpenPubDTO), Encoding.UTF8, "application/json");

                using (HttpResponseMessage responseUpdate = await objHttpClient.PostAsync(strClosePublicationURL, content))
                {
                    if (!responseUpdate.IsSuccessStatusCode)
                    {
                        isPublicationClosed = false;
                    }
                    else
                    {
                        isPublicationClosed = true;
                    }
                }
            }
        }
        return isPublicationClosed;
    }
}