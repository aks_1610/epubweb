﻿<%@ Page Async="true" Language="C#" AutoEventWireup="true" CodeFile="simple_document.aspx.cs" EnableEventValidation="false" Inherits="simple_document" %>
<%@ Import Namespace="lib" %>
<%@ Register TagPrefix="flowpaper" TagName="annotations_handler" Src="~/annotations_handlers.ascx" %>
<!doctype html>
<html>
	<head> 
		<title>Rawlinsons ePub Application</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
		<meta http-equiv="EXPIRES" content="0">
		<link rel="stylesheet" type="text/css" href="css/flowpaper_flat.css" />
		<%
				
				if (Session["APIClasses"] != null && !(Session["APIClasses"] as APIClasses).SecretModeOn)
				{
					%>
		<link rel="stylesheet" href="css/bootstrap-iso.css" />
		<%}%>
		<link rel="shortcut icon" type="image/x-icon" href="images/rawlinsons_kcv_icon.ico" />
		<style type="text/css" media="screen"> 
			html, body	{ height:100%; }
			body { margin:0; padding:0; overflow:auto; }   
			#flashContent { display:none; }
			textarea {
				margin: 0;
				font: inherit;
			}

			.button {
				color: rgba(255,255,255,1) !important;
				font-weight: normal !important;
				background: #008071 !important;
				height: 30px !important;
				width:80px !important;
				border:none;
			}
			.buttonWhite {
				color: #FFFFFF;
				background: transparent;
				font-size: 14px;
				height: 30px !important;
				width:80px !important;
				border:none;
			}
			.topbar_image {
				background: #008071;
				height: 35px;
				width: 35px;
				border-radius: 50%;
				padding: 5px;
				margin-right: 6px;
				display: flex;
				align-items: center;
				justify-content: center;
			}
			.img_rounded
			{
				border-radius: 50%;
			}
		</style>
		<%
				
				if (Session["APIClasses"] != null && !(Session["APIClasses"] as APIClasses).SecretModeOn)
				{
					%>
		<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
		<%}else{%>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<%}%>
		<script type="text/javascript" src="js/jquery.extensions.min.js"></script>
		
		<script type="text/javascript" src="js/flowpaper.js"></script>
		<script type="text/javascript" src="js/flowpaper_handlers.js"></script>
		<%
				
				if (Session["APIClasses"] != null && !(Session["APIClasses"] as APIClasses).SecretModeOn)
				{
					%>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript">
			var sessionTimeout = 1 * 60000;
			function ConfimClose() {
				var returnConfirm = confirm("Are you sure, you want to close this application?");
				if (returnConfirm) {
					stopIdleTime();
				}
				return returnConfirm;
			}
		</script>
		<%}%>
		<script type="text/javascript">
			window.setInterval("renewSession();", 600000);
			function renewSession()
			{ document.images("renewSession").src = "/renewSes.aspx?par=" + Math.random(); }
		</script>
	</head> 
	<body>
		<form runat="server">
			<input type="hidden" id="count" />
			<div id="pageDiv" runat="server">
				<table width="99%" cellpadding='15' cellspacing='0'>
					<tr>
						<td width="50%">
							<img height="58" alt="Rawlinsons ePub" src="images/logo.png" />
							<img src="/renewSes.aspx" name="renewSession" id="renewSession">
						</td>
						<td width="50%" align="right">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td align="right" >
										<table>
											<tr>
												<td>
													<span class="topbar_image"> 
														<img runat="server" id="userImg" src="/images/default_user.png" class="img_rounded" width="20">
													</span>
												</td>
												<td>
													<div id="lblUser" runat="server" style="font:bold 14px arial,verdana"></div>
												</td>
											</tr>
										</table>      
									</td>
								</tr>
								<tr>
									<td align="right">
										<asp:Button ID="btnClose" class="button" runat="server" Text="Close" OnClick="btnClose_Click" OnClientClick="return ConfimClose()" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<%
				
				if (Session["APIClasses"] != null && !(Session["APIClasses"] as APIClasses).SecretModeOn)
				{
					%>
			<div class="bootstrap-iso">
				<div class="modal fade" id="session-expire-warning-modal" aria-hidden="true" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">

						<div class="modal-content">

							<div class="modal-header">                 

								<h4 class="modal-title">Session Expire Warning</h4>

							</div>

							<div class="modal-body">

								Your session will expire in <span id="seconds-timer"></span> seconds. Do you want to extend the session?

							</div>

							<div class="modal-footer">

								<button id="btnOk" type="button" class="button" style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; border: 1px solid transparent; border-radius: 4px;  background-color: #428bca; color: #FFF;">Yes</button>
								<button id="btnLogoutNow" type="button" class="buttonWhite" style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; border: 1px solid transparent; border-radius: 4px;  background-color: #FFF; color: #428bca;">No</button>

							</div>

						</div>

					</div>
				</div>
				<div class="modal fade" id="session-expired-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Session Expired</h4>
							</div>
							<div class="modal-body">
								Your session is expired.
							</div>
							<div class="modal-footer">
								<button id="btnExpiredOk" onclick="sessLogOut()" type="button" class="button" data-dismiss="modal" style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; border: 1px solid transparent; border-radius: 4px; background-color: #428bca; color: #FFF;">Ok</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<% } %>
		</form>
		<%
			var height = "97%";
			if (Session["APIClasses"] != null)
			{
				if (!(Session["APIClasses"] as APIClasses).SecretModeOn)
				{
					height = "81%";
				}
				%>
			<input type="hidden" id="openPublicationJSONDTO" runat="server" />
			<input type="hidden" id="closePublicationURL" runat="server" />
			<div id="documentViewer" class="flowpaper_viewer" style="position:absolute;left:10px;width:99%;height:<%=height%>"></div>
		   
			<script type="text/javascript">   
				function getDocQueryServiceUrl(document){
					return "services/swfsize.ashx?doc={doc}&page={page}".replace("{doc}",document);
				}
				
				var startDocument = "<% if (null!=Session["APIClasses"] && (Session["APIClasses"] as APIClasses).Publication.pubFilename != null) { Response.Write((Session["APIClasses"] as APIClasses).Publication.pubFilename); }else{%>Paper.pdf<% } %>";

				function append_log(msg){
					$('#txt_eventlog').val(msg+'\n'+$('#txt_eventlog').val());
				}

				String.format = function () {
					var s = arguments[0];
					for (var i = 0; i < arguments.length - 1; i++) {
						var reg = new RegExp("\\{" + i + "\\}", "gm");
						s = s.replace(reg, arguments[i + 1]);
					}

					return s;
				}

					<flowpaper:annotations_handler id="annotations_handler1" runat="server" />

						jQuery.get((!window.isTouchScreen) ? 'UI_flowpaper_desktop_flat.html' : 'UI_flowpaper_mobile.html',
							function (toolbarData) {
								$('#documentViewer').FlowPaperViewer(
									{
										config: {
											PDFFile: "services/view.ashx?doc=" + startDocument + "&format=pdf&page={page}",
											Scale: 1.5,
											ZoomTransition: 'easeOut',
											ZoomTime: 0.5,
											ZoomInterval: 0.1,
											FitPageOnLoad: false,
											FitWidthOnLoad: false,
											FullScreenAsMaxWindow: false,
											ProgressiveLoading: false,
											MinZoomSize: 0.2,
											MaxZoomSize: 5,
											SearchMatchAll: false,
											InitViewMode: 'Portrait',
											RenderingOrder: '<%=(configManager.getConfig("renderingorder.primary") + ',' + configManager.getConfig("renderingorder.secondary")) %>',
											ViewModeToolsVisible: true,
											ZoomToolsVisible: true,
											NavToolsVisible: true,
											CursorToolsVisible: true,
											SearchToolsVisible: true,
											Toolbar: toolbarData,
											StickyTools: true,
											BottomToolbar: 'UI_flowpaper_annotations.html',

											DocSizeQueryService: 'services/swfsize.ashx?doc=' + startDocument,

											JSONDataType: 'jsonp',
											JSONFile: '',
											key: '$f4f270a8acee63520ad',

											localeChain: 'en_US'

										}
									}
								);
							});
			</script>
				<% if (configManager.getConfig("sql.verified") == "true")
					{ %>
				<div style="visibility:hidden;position:absolute;left:1250px;top:10px;font-family:Arial;font-size:12px"><b>Database Event Log</b><br/><textarea rows=12 cols=28 id="txt_eventlog" style="width:570px;font-size:9px;" wrap="off"></textarea></div>
				<% }
			}
			else
			{
				Response.Write("<H2>User session not found.</h2>");
				Response.End();
			}%>
		 <%
			 if (Session["APIClasses"] != null && !(Session["APIClasses"] as APIClasses).SecretModeOn)
			 {%>
				<script type="text/javascript">
					sessionTimeout = "<%= (Session["APIClasses"] as APIClasses).SessionExpireTime %>"
					sessionTimeout = (parseInt(sessionTimeout) -6) * 60000;
				</script>
				<script type="text/javascript" src="js/sessionexpiration.js"></script>
					<!-- This will init session -->
				<script type="text/javascript">
						initSessionMonitor();
				</script>
		<%}%>
   </body>
</html> 