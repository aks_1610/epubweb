﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

public partial class _Default : System.Web.UI.Page

{
    #region Private variables

    private HttpClient objHttpClient = null;
    private bool isAuthenticated;
    private string strAPIMessage = String.Empty;
    private APIClasses objAPIClasses = null;

    #endregion

    /// <summary>
    /// _Default
    /// </summary>
    public _Default()
    {
        objAPIClasses = new APIClasses();
    }

    /// <summary>
    /// Page_Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected async void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString.Count <= 0)
            {
                Response.Write("<H2><font color='red'>Application can not continue as valid arguments where not passed.</font></H2>");
            }
            else
            {
                String[] objUserTokenArr = Request.QueryString[0].Split(':');

                objAPIClasses.UserToken = objUserTokenArr[1].Replace(" ","+");
                objAPIClasses.PublicationId = int.Parse(objUserTokenArr[2]);
                objAPIClasses.BaseAddressV1 = String.Empty;

                if (objUserTokenArr[4].ToLower() == "//localhost")
                {
                    objAPIClasses.BaseAddressV1 = String.Format("{0}:{1}:{2}", objUserTokenArr[3], objUserTokenArr[4], objUserTokenArr[5]);
                    objAPIClasses.SecretModeOn = objUserTokenArr[6].ToLower() == "on" ? true : false;
                }
                else
                {
                    objAPIClasses.BaseAddressV1 = String.Format("{0}:{1}", objUserTokenArr[3], objUserTokenArr[4]);
                    objAPIClasses.SecretModeOn = objUserTokenArr[5].ToLower() == "on" ? true : false;
                }


                objHttpClient = new HttpClient();
                objHttpClient.BaseAddress = new Uri(objAPIClasses.BaseAddressV1);
                objHttpClient.DefaultRequestHeaders.Accept.Clear();
                objHttpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                await Authenticate();

                if (isAuthenticated)
                {
                    String strSimpleDocURL = "simple_document.aspx";// String.Format("simple_document.aspx?doc={0}&publicationId={1}&userId={2}&companyId={3}", objAPIClasses.Publication.pubFilename, objAPIClasses.PublicationId, objAPIClasses.User.id, objAPIClasses.User.companyID);
                    objAPIClasses.HttpClient = objHttpClient;
                    Session["APIClasses"] = objAPIClasses;
                    //Session.Timeout = objAPIClasses.SessionExpireTime;
                    Response.Redirect(strSimpleDocURL, false);
                    
                    //causes ASP.NET to bypass all events and filtering in the HTTP pipeline
                    //chain of execution and directly execute the EndRequest event
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    Response.Write(string.Format("<h2><font color='red'>{0}</font></h2>", strAPIMessage));
                }
            }
        }
        catch(Exception ex)
        {
            //Do nothing
        }
    }

    #region Private methods
    /// <summary>
    /// Authenticate
    /// </summary>
    /// <returns>Task</returns>
    private async Task Authenticate()
    {
        bool isAuthenticated = false;
        try
        {
            string strAuthenticationUrl = ConfigurationManager.AppSettings.Get("AuthenticationUrl");

            var objAuthenticateJSON = new
            {
                ClientID = ConfigurationManager.AppSettings.Get("ClientID"),
                ClientSecret = ConfigurationManager.AppSettings.Get("ClientSecret"),
                DeviceType = ConfigurationManager.AppSettings.Get("DeviceType"),
                Id = int.Parse(ConfigurationManager.AppSettings.Get("Id"))
            };

            StringContent content = new StringContent(JsonConvert.SerializeObject(objAuthenticateJSON), Encoding.UTF8, "application/json");

            ClientTokenKey objClientTokenKey = null;

            using (HttpResponseMessage response = await objHttpClient.PostAsync(strAuthenticationUrl, content))
            {
                if (response.IsSuccessStatusCode)
                {
                    string data = await response.Content.ReadAsStringAsync();
                    objClientTokenKey = JsonConvert.DeserializeObject<ClientTokenKey>(data);

                    //Set the Client Token key
                    objAPIClasses.ClientToken = objClientTokenKey.tokenKey;

                    objHttpClient.DefaultRequestHeaders.Clear();

                    //Add authentication headers
                    objHttpClient.DefaultRequestHeaders.Add("TokenKey", objClientTokenKey.tokenKey);
                    objHttpClient.DefaultRequestHeaders.Add("UserToken", objAPIClasses.UserToken);

                    var strConfigParameterControllerURL = string.Concat(ConfigurationManager.AppSettings.Get("ConfigParameterControllerURL"), ConfigurationManager.AppSettings.Get("UserTokenExpiry"));

                    //Get the company details
                    using (HttpResponseMessage respnseConfigParameter = await objHttpClient.GetAsync(strConfigParameterControllerURL))
                    {
                        if (respnseConfigParameter.IsSuccessStatusCode)
                        {
                            data = await respnseConfigParameter.Content.ReadAsStringAsync();
                            objAPIClasses.ConfigParameter = JsonConvert.DeserializeObject<ConfigParameter>(data);

                            objAPIClasses.SessionExpireTime = int.Parse(objAPIClasses.ConfigParameter.parameterValue);

                            isAuthenticated = true;
                        }
                        else
                        {
                            isAuthenticated = false;
                        }
                    }
                }
                else
                {
                    isAuthenticated = false;
                    strAPIMessage = "User can not be authenticated.";
                }
            }

            if (objClientTokenKey != null && isAuthenticated)
            {
                var objUserTokenURLJSON = new { userToken = objAPIClasses.UserToken };
                string UserTokenURL = ConfigurationManager.AppSettings.Get("UserTokenURL");

                var Usercontent = new StringContent(JsonConvert.SerializeObject(objUserTokenURLJSON), Encoding.UTF8, "application/json");
                

                //Get the user details
                using (HttpResponseMessage responseUser = await objHttpClient.PostAsync(UserTokenURL, Usercontent))
                {
                    if (responseUser.IsSuccessStatusCode)
                    {
                        string data = await responseUser.Content.ReadAsStringAsync();
                        objAPIClasses.User = JsonConvert.DeserializeObject<User>(data);
                        isAuthenticated = true;
                    }
                    else
                    {
                        isAuthenticated = false;
                        objAPIClasses.User = null;
                    }
                }

                if (isAuthenticated)
                {
                    var strPublicationURL = string.Format(ConfigurationManager.AppSettings.Get("PublicationURL"), objAPIClasses.PublicationId);
                    //Get the publication details
                    using (HttpResponseMessage responsePublication = await objHttpClient.GetAsync(strPublicationURL))
                    {
                        if (responsePublication.IsSuccessStatusCode)
                        {
                            string data = await responsePublication.Content.ReadAsStringAsync();
                            objAPIClasses.Publication = JsonConvert.DeserializeObject<Publication>(data);
                            isAuthenticated = true;
                        }
                        else
                        {
                            strAPIMessage = "Publication details not available.";
                            isAuthenticated = false;
                        }
                    }
                }
                else
                {
                    strAPIMessage = "Authentication was not successful.";
                }
            }
        }
        catch (Exception ex)
        {
            strAPIMessage = "Authentication was not successful.";
            isAuthenticated = false;
        }

        this.isAuthenticated = isAuthenticated;
    }

    #endregion
}