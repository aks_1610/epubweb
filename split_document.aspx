﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="split_document.aspx.cs" Inherits="split_document" %>
<%@ Import Namespace="lib" %>
<%@ Register TagPrefix="flowpaper" TagName="annotations_handler" Src="~/annotations_handlers.ascx" %>
<!doctype html>
<html>
    <head> 
        <title>FlowPaper</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        <style type="text/css" media="screen"> 
			html, body	{ height:100%; }
			body { margin:0; padding:0; overflow:auto; }   
			#flashContent { display:none; }
        </style> 
		
		<link rel="stylesheet" type="text/css" href="css/flowpaper_flat.css" />
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery.extensions.min.js"></script>
		<script type="text/javascript" src="js/flowpaper.js"></script>
		<script type="text/javascript" src="js/flowpaper_handlers.js"></script>
    </head> 
    <body>
        <%
			// Setting current document from parameter or defaulting to 'Paper.pdf'
			String doc = "Paper.pdf";
			if(Request["doc"]!=null)
			    doc = Request["doc"].ToString();
			
			String pdfFilePath = configManager.getConfig("path.pdf") + doc;
			String swfFilePath = configManager.getConfig("path.swf");
		%>
			<div id="documentViewer" class="flowpaper_viewer" style="position:absolute;left:10px;top:10px;width:800px;height:500px"></div>

	        <script type="text/javascript">
                var numPages 			= <%=Common.getTotalPages(pdfFilePath,swfFilePath,doc) %>;

		        function getDocumentUrl(document){
					var url = "{services/view.ashx?doc={doc}&format={format}&page=[*,0],{numPages}}";
						url = url.replace("{doc}",document);
						url = url.replace("{numPages}",numPages);
						return url;	        
		        }

		        function append_log(msg){
                    $('#txt_eventlog').val(msg+'\n'+$('#txt_eventlog').val());
                }

                String.format = function() {
                    var s = arguments[0];
                    for (var i = 0; i < arguments.length - 1; i++) {
                        var reg = new RegExp("\\{" + i + "\\}", "gm");
                        s = s.replace(reg, arguments[i + 1]);
                    }

                    return s;
                }

                var startDocument       = "<% if(Request["doc"]!=null){Response.Write(Request["doc"]);}else{%>Paper.pdf<% } %>";
		        var swfFileUrl 			= escape('{services/view.ashx?doc='+startDocument+'&page=[*,0],'+numPages+'}');
				var searchServiceUrl	= escape('aspnet/services/containstext.ashx?doc='+startDocument+'&page=[page]&searchterm=[searchterm]');

                <% if(configManager.getConfig("sql.verified") == "true"){ %>
                <flowpaper:annotations_handler id="annotations_handler1" runat="server" />
                <% } %>

                function saveAnnotatedPDF(asPrint){
                    var doc = createjsPDFDoc($FlowPaper('documentViewer').getMarkList(),numPages);

                    $.post( "services/annotate.ashx", { 'doc' : '<%=doc%>', 'subfolder' : '', 'stamp' :  doc.output('datauristring'), 'print' : asPrint},function(data){
                        if(data.indexOf("[Error") == -1){
                           $('.flowpaper_printframe').remove();
                           $("body").append("<iframe id='printFrame' src='" + data + "' class='flowpaper_printframe' style='display: none;' ></iframe>");
                           $('#printFrame').on('load', function(){
                                var iframe = document.getElementById('printFrame');
                                var ifWin = iframe.contentWindow || iframe;

                                try{
                                    iframe.focus();
                                    ifWin.print();
                                    return false;
                                }catch(e){
                                    window.open(data,'_flowpaper_printurl');
                                }
                           });
                        }else{
                            alert(data);
                        }
                    });
                }

                jQuery.get((!window.isTouchScreen)?'UI_flowpaper_desktop_flat.html':'UI_flowpaper_mobile.html',
                function(toolbarData) {
				$('#documentViewer').FlowPaperViewer(
				  { config : {
						 
						 DOC                        : escape(getDocumentUrl(startDocument)),
						 Scale                      : 0.6,
						 ZoomTransition             : 'easeOut',
						 ZoomTime                   : 0.5,
						 ZoomInterval               : 0.1,
						 FitPageOnLoad              : true,
						 FitWidthOnLoad             : false,
						 FullScreenAsMaxWindow      : false,
						 ProgressiveLoading         : false,
						 MinZoomSize                : 0.2,
						 MaxZoomSize                : 5,
						 SearchMatchAll             : false,
  						 SearchServiceUrl           : searchServiceUrl,
						 InitViewMode               : 'Portrait',
						 RenderingOrder             : '<%=(configManager.getConfig("renderingorder.primary") + ',' + configManager.getConfig("renderingorder.secondary")) %>',
						 
						 ViewModeToolsVisible       : true,
						 ZoomToolsVisible           : true,
						 NavToolsVisible            : true,
						 CursorToolsVisible         : true,
						 SearchToolsVisible         : true,

                         StickyTools                : true,
                         Toolbar                    : toolbarData,
                         BottomToolbar              : 'UI_flowpaper_annotations.html',

  						 key                        : '<%=configManager.getConfig("licensekey") %>',

  						 PrintFn                    : function(){saveAnnotatedPDF(true)},

  						 DocSizeQueryService        : 'services/swfsize.ashx?doc=<%=doc %>',
						 JSONDataType               : 'jsonp',
						   						 
  						 localeChain                : 'en_US'
						 }}
                    );
                });
	        </script>

            <div style="position:absolute;left:830px;top:10px;font-family:Arial;font-size:12px">
                <% if(configManager.getConfig("sql.verified") == "true"){ %>
                <div style="font-family:Arial;font-size:12px"><b>Database Event Log</b><br/><textarea rows=6 cols=28 id="txt_eventlog" style="width:370px;font-size:9px;" wrap="off"></textarea></div>
                <% } %>
                <br/><input type="submit" value="Download document as annotated PDF" onclick="saveAnnotatedPDF();">
            </div>
   </body> 
</html> 